<?php
/**
 * @file
 * basic_cart_order_stripe.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function basic_cart_order_stripe_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer basic cart stripe.
  $permissions['administer basic cart stripe'] = array(
    'name' => 'administer basic cart stripe',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'basic_cart',
  );

  // Exported permission: use basic cart stripe.
  $permissions['use basic cart stripe'] = array(
    'name' => 'use basic cart stripe',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
    ),
    'module' => 'basic_cart',
  );

  // Exported permission: view basic cart stripe orders.
  $permissions['view basic cart stripe orders'] = array(
    'name' => 'view basic cart stripe orders',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'basic_cart',
  );

  return $permissions;
}
